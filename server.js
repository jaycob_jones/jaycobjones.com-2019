const express = require('express')
const next = require('next')
const bodyParser = require("body-parser");



const sgMail = require("@sendgrid/mail");
var randomstring = require("randomstring");

const port = parseInt(process.env.PORT, 10) || 3000
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()


app.prepare().then(() => {
  const server = express()

  server.use(bodyParser.json());

  server.get("*", (req, res) => {
    return handle(req, res);
  });

  server.post("/api/contact", (req, res) => {
    const { email, name, message } = req.body;

    sgMail.setApiKey(process.env.SENDGRID_API_KEY);
    const msg = {
      to: ["jonesjaycob@gmail.com", email],
      from: "noreply@jaycobjones.com",
      subject: "Your email confirmation from jaycobjones.com",
      text: "Jaycob Jones is a digital marketing expert",
      html: `
      <strong>Name:</strong> ${name} <br/>
      <strong>Email:</strong> ${email} <br/>
      <br/>
      <strong>Message:</strong> ${message} <br/><br/> 

      <strong>Here is your conformation code:</strong> ${randomstring.generate(13)}
      `
    };
    sgMail
      .sendMultiple(msg)
      .then(res => {
         console.log(res);
      })
      .catch(error => {
        //Log friendly error
        console.error(error.toString());

        //Extract error msg
        const { message, code, response } = error;

        //Extract response msg
        const { headers, body } = response;
      });
      req.email = 'winnerrr'
    res.send('api/contact');
  });

  server.listen(port, err => {
    if (err) throw err
    console.log(`> Ready on http://localhost:${port}`)
  })
});
