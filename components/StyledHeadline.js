import styled from "styled-components";

const Headline = styled.div`
  position: relative;
  .standard {
    position: relative;
    .bigGuy {
      font-size: 13rem;
      color: ${props => props.theme.colors.white};
      text-shadow: 2px 2px 20px rgba(0, 0, 0, 0.3);
    }
    .littleGuy {
      font-size: 7rem;
      color: ${props => props.theme.colors.primaryColor};
      position: absolute;
      top: 120px;
      left: 0;
      z-index: 2;
    }
  }

  .switchColors {
    position: relative;
    .littleGuy {
      font-size: 7rem;
      color: ${props => props.theme.colors.white};
      position: absolute;
      top: 120px;
      left: 0;
      z-index: 2;
    }
    .bigGuy {
      font-size: 13rem;
      color: ${props => props.theme.colors.primaryColor};
      text-shadow: 2px 2px 20px rgba(0, 0, 0, 0.3);
    }
  }
`;

const StyledHeadline = props => (
  <Headline>
    <div className={props.inverted ? "switchColors" : "standard"}>
      <div className="bigGuy">{props.label}</div>
      <div className="littleGuy">{props.label}</div>
    </div>
  </Headline>
);

export default StyledHeadline;
