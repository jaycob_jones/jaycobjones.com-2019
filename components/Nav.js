import React from "react";
import styled from "styled-components";
import Link from "next/link";

const Navigation = styled.div`
  display: grid;
  grid-template-columns: 1fr 9fr;

  @media (max-width: ${props => props.theme.breakpoints.xl}) {
    grid-template-columns: 1fr 7fr;
  }

  @media (max-width: ${props => props.theme.breakpoints.l}) {
    grid-template-columns: 1fr 6fr;
  }
  @media (max-width: ${props => props.theme.breakpoints.m}) {
    grid-template-columns: 1fr 5fr;
  }
  @media (max-width: ${props => props.theme.breakpoints.s}) {
    grid-template-columns: 1fr 2fr;
  }

  .logo {
    margin-top: 10px;

    @media (max-width: ${props => props.theme.breakpoints.s}) {
      width:60px;
    }
  }
  a {
    margin: 10px 20px;
    color: ${props => props.theme.colors.white};
  }
  div {
    float: right;
  }

  img {
    cursor: pointer;
  }
`;

const Nav = () => (
  <Navigation>
    <Link href="/">
      <img className="logo" src="../static/jaycobs-logo.svg" />
    </Link>
    <div>
      <div>
        <Link href="/#specialities">
          <a>Specialities</a>
        </Link>
        <Link href="/#work">
          <a>Work</a>
        </Link>
        <Link href="/#contact">
          <a>Contact</a>
        </Link>
      </div>
    </div>
  </Navigation>
);

export default Nav;
