import styled from 'styled-components';


const ButtonStyle = styled.div`
margin-top:10px;
    a {
        cursor: pointer;
        text-transform: uppercase;
        padding: 10px 30px;
        background-color:white;
        border-radius:30px;
        transition: background-color 500ms ease;
        border: solid 2px ${props => props.theme.colors.white};
        &:hover{
            cursor: pointer;
            background-color:${props => props.theme.colors.primaryColor};
            border: solid 2px ${props => props.theme.colors.buttonOn};
            transition: background-color 500ms ease;
            color:${props => props.theme.colors.white};
        }
    }
    span {
        pointer-events: none;
    }
`;
const Button = (props) => (
    <ButtonStyle>
        <a href={props.href}>
            <span>{props.label}</span>
        </a>
  </ButtonStyle>
);


export default Button;