import React, { Component } from "react";
import styled from "styled-components";
import Link from "next/link";

const CookieMonster = styled.div`
  position: fixed;
  bottom: 0;
  left: 0;
  width: 300px;
  margin: 20px;
  z-index: 5;
  background-color: rgba(255, 255, 255, 0.9);
  padding: 10px;
  box-shadow: 2px 2px 10px rgba(0, 0, 0, 0.2);
  span {
    color: ${props => props.theme.colors.primaryColor};
    border: 1px solid ${props => props.theme.colors.primaryColor};
    padding: 4px 7px;
    font-size: 12px;
    cursor: pointer;
  }
  a{
    text-decoration: underline;
  }
`;

class CookieAlert extends Component {
  constructor() {
    super();

    this.state = {
      cookieShow: true
    };
  }

  render() {
    if (this.state.cookieShow)
      return (
        <CookieMonster>
          We use cookies to help our site work. By clicking “Accept” below, you
          agree to us doing so. You can read more in our cookie notice{" "}
          <Link href="/cookies">
            <a>here.</a>
          </Link>
          <br /><br />
          <span
            onClick={e => {
              this.setState({
                cookieShow: !this.state.cookieShow
              });
            }}
          >
            Accept
          </span>
        </CookieMonster>
      );

    return null;
  }
}

export default CookieAlert;
