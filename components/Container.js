import styled from "styled-components";

const SectionContainer = styled.section`
  overflow: hidden;
  position: relative;
  width: 100%;
  padding: 10px 30px;
  .inner-container {
    width: 100%;
    max-width: ${props => props.theme.default.siteContainer};
    margin: 0 auto;
  }
`;
const Container = props => (
  <SectionContainer id={props.id ? props.id : ""} className={props.className}>
    <div className="inner-container">{props.children}</div>
  </SectionContainer>
);

export default Container;
