import React, { Component } from "react";
import styled from "styled-components";
import StyledHeadline from "./StyledHeadline";
import Container from "./Container";
import PortfolioImage from "./PortfolioImage";
import Button from "./Button";

const WorkSection = styled.div`
  position: relative;
  .inverted-divider {
    position: absolute;
    top: -15px;
    left: -160px;
    width: 130%;

    @media (max-width: ${props => props.theme.breakpoints.m}) {
      left: -100px;
  }
  @media (max-width: ${props => props.theme.breakpoints.s}) {
  left: -100px;  
  }
  }
  .clear-divider {
    padding-top: 130px;
    position: relative;
  }
`;

const WorkContainers = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  margin-top: 90px;
  grid-column-gap: 20px;
  grid-row-gap: 20px;
  margin-bottom: 90px;

  @media (max-width: ${props => props.theme.breakpoints.m}) {
    grid-template-columns: 1fr 1fr ;
  }
  @media (max-width: ${props => props.theme.breakpoints.s}) {
    grid-template-columns: 1fr  ;
  }
`;

const LineArtCSS = styled.div`
  position: absolute;
  top: 270px;
  left: 110px;
  width: 600px;
  z-index: 1;

  .img {
    width: 100%;
  }
`;
const BTNContainer = styled.div`
  text-align: center;
  margin-bottom: 90px;
`;



class Work extends Component {
  constructor() {
    super();

    this.state = {
      work: [
        {
          title: "Braclet Shape Roller",
          url: "../static/bra1.gif",
        }, 
        {
          title: "Braclet Clasp Twister",
          url: "../static/bra2.gif",
        },  
        {
          title: "Braclet Male Clasp Maker",
          url: "../static/bra3.gif",
        },  
        {
          title: "Braclet Female Clasp Maker",
          url: "../static/bra4.gif",
        },   
        {
          title: "Deck Design and Build",
          url: "../static/deck.jpg",
        },  
        {
          title: "Wakuui Leveling System",
          text:
            "Wakuui is the first of its kind, an affordable automated self-leveling system for the overhead tent enthusiasts. Our product turns your static roof rack into a state of the art dynamic system with high pressured pneumatic cylinders. The high pressured pneumatic cylinders are quick to respond so that your tent system will be leveled in a moment’s time.",
          url: "../static/wakuui.jpg",
          link: "http://wakuui.weebly.com/"
        },  
        {
          title: "Aluminum Pen",
          text:
            "",
          url: "../static/pen.jpg",
          link: ""
        },  
         
        {
          title: "Shop Shelf",
          text:
            "",
          url: "../static/shopshelf.jpg",
          link: ""
        },   
        {
          title: "Research Specimen",
          text:
            "",
          url: "../static/specimen1.jpg",
          link: ""
        }, 
        {
          title: "Rocket Stand Fabrication",
          text:
            "",
          url: "../static/rockety.jpg",
          link: ""
        }, 
        {
          title: "Research Tube Holder",
          text:
            "",
          url: "../static/glassholder.jpg",
          link: ""
        },   
        {
          title: "Soft Jaws",
          text:
            "",
          url: "../static/softjaws.jpg",
          link: ""
        },
        {
          title: '.001" Tolerance',
          text:
            "",
          url: "../static/1thou.jpg",
          link: ""
        }, 
        {
          title: "Custom Bed and Shelves",
          text:
            "",
          url: "../static/bed.jpg",
          link: ""
        }, 
        {
          title: "Herb Stand",
          text:
            "",
          url: "../static/HerbStand.jpg",
          link: ""
        }, 
        {
          title: "9 DOF IMU",
          text:
            "",
          url: "../static/9dof.jpg",
          link: ""
        },  
        {
          title: "",
          text:
            "",
          url: "../static/angle.jpg",
          link: ""
        },  
         
      ],
      toShow: 6,
      expanded: false
    };
  }
  render() {
    const handleClick = () => {
      if (this.state.toShow <= this.state.work.length) {
        const current = this.state.toShow + 3;
        this.setState({
          toShow: current,
          expanded:
          current >= this.state.work.length ? true : false
        });
      }
    };

    return (
      <Container id='work' className="background-lightGrey">
        <WorkSection>
          <img
            className="inverted-divider"
            src="../static/section-divider-inverted.svg"
          />
          <div className="clear-divider">
            <StyledHeadline label="Work" />
            <LineArtCSS>
              <img src="../static/line-art-2.svg" />
            </LineArtCSS>
          </div>
        </WorkSection>
        <WorkContainers>
          {this.state.work.map((value, i) => {
            if (i < this.state.toShow)
              return (
              
                <PortfolioImage
                  key={i}
                  src={value.url}
                  title={value.title}
                  text={value.text}
                  link={value.link ? value.link : null }
                />
                
               
              );
          })}
        </WorkContainers>
        {this.state.expanded ? (
          ""
        ) : (
          <BTNContainer>
          <span className='btn' onClick={handleClick}>
            <Button label="Show More" />
          </span>
          </BTNContainer>
        )}
      </Container>
    );
  }
}

export default Work;
