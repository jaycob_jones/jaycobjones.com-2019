import Container from "./Container";
import styled from "styled-components";
import StyledHeadline from "./StyledHeadline";
import ContactForm from "./ContactForm";
import { FaEnvelope, FaPhone } from 'react-icons/fa';

import ContactCallout from './ContactCallout';

const ContactSection = styled.div`
  margin-top: 90px;
  position: relative;
  margin-bottom:50px;
`;

const LineArtCSS = styled.div`
  position: absolute;
  top: 0px;
  left: 80px;
  width: 250px;
  z-index: 1;

  .img {
    width: 100%;
  }
`;

const FormatSection = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  padding: 0 60px;
  grid-column-gap: 180px;

  @media (max-width: ${props => props.theme.breakpoints.m}) {
    grid-template-columns: 1fr;
  }
`;
const Contact = () => (
  <Container id='contact' className="background-primaryColor">
    <ContactSection>
      <StyledHeadline label="Contact" inverted="true" />
      <LineArtCSS>
        <img src="../static/line-art-3.svg" />
      </LineArtCSS>
      <FormatSection>
      <ContactForm />
      <div>
        <ContactCallout label='Email' info='jonesjaycob@gmail.com' link='mailto:jonesjaycob@gmail.com'>
          <FaEnvelope />
        </ContactCallout>
        <ContactCallout label='Phone' info='775.848.8609' link='tel:7758488609'>
          <FaPhone />
        </ContactCallout>
      </div>
      
      </FormatSection>
    </ContactSection>
  </Container>
);

export default Contact;
