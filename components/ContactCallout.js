import React from "react";
import styled from "styled-components";

const InfoSection = styled.div`
  padding: 30px 0 30px 30px;
  border-top: 1px solid ${props => props.theme.colors.buttonOn};

  &:last-child {
    border-bottom: 1px solid ${props => props.theme.colors.buttonOn};
  }

  &:first-child {
    margin-top: 30px;
  }

  .label {
    color: ${props => props.theme.colors.white};
    font-size: 2rem;
    span {
      background-color: ${props => props.theme.colors.buttonOn};

      padding: 5px 10px;
      margin-right: 15px;
      border-radius: 50%;
    }
  }
  .link {
    margin-top: 13px;
    margin-left: 55px;
    a {
      color: ${props => props.theme.colors.white};
    }
  }
`;

const ContactCallout = props => {
  return (
    <InfoSection>
      <div className="label">
        <span>{props.children}</span>
        {props.label}
      </div>
      <div className="link">
        <a href={props.link}>{props.info}</a>
      </div>
    </InfoSection>
  );
};

export default ContactCallout;
