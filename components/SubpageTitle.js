import React from 'react';
import Container from './Container';
import Nav from './Nav';
import styled from 'styled-components';


const HeadlineStyle = styled.div`
    width: 100%;
    display: block;
    text-align: center;
    margin: 40px 0 20px 0;
    span {
        font-size: 7rem;
        font-weight: bold;
        position:relative;
        margin: 0 auto;
        display:inline;
        &::before{
            position:absolute;
            top:60px;
            left:-47px;
            width: 30px;
            height: 3px; 
            content: ' ';
            background-color: ${props => props.theme.colors.white}
        }
        &::after{
            position:absolute;
            top:60px;
            right:-47px;
            width: 30px;
            height: 3px; 
            content: ' ';
            background-color: ${props => props.theme.colors.white}
        }
    }
`;
const SubpageTitle = (props) => {
    return (
        <Container className='background-primaryColor'>
            <Nav />
            <HeadlineStyle>
                <span className='text-center color-white'>
                {props.pageTitle}
                </span>
            </HeadlineStyle>
        </Container>
    );
};

export default SubpageTitle;