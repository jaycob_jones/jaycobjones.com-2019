import React, { Component } from "react";
import { Form, Text, TextArea } from "informed";
import "isomorphic-fetch";
import styled from "styled-components";

const FormContainer = styled.div`
  position: relative;
  input,
  textarea {
    padding: 10px 15px;
    border-radius: 25px;
    border: 1px solid ${props => props.theme.colors.buttonOn};
    width: 100%;
    background: ${props => props.theme.colors.buttonOn};
    color: ${props => props.theme.colors.white}
  }
  textarea{
      min-height:200px;
      max-width: 100%;
  }
  label {
    padding-top: 20px;
    position: relative;
    color:${props => props.theme.colors.white};
  }

  button {
    background: ${props => props.theme.colors.primaryColor};
    font-family: "Oswald", sans-serif;
    color: ${props => props.theme.colors.white};
    font-size: 1.5rem;
    padding: 10px 30px;
    border-radius: 25px; 
    transition: background-color 500ms ease;
    text-transform: uppercase;
    &:hover{
        border: 1px solid ${props => props.theme.colors.buttonOn};
        transition: background-color 500ms ease;
        background: ${props => props.theme.colors.buttonOn};
        color: ${props => props.theme.colors.primaryColor};
      
    }
  }
`;

class ContactForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      submitting: false,
      submitted: false
    };
  }

  render() {
    const formSubmit = formState => {
      if (
        formState.touched.email &&
        formState.touched.name &&
        formState.touched.message == true
      ) {
        fetch("/api/contact", {
          method: "post",
          headers: {
            Accept: "application/json, text/plain, */*",
            "Content-Type": "application/json"
          },
          body: JSON.stringify(formState.values)
        }).then(res => {
          console.log(res);
          res.status == 200 && this.setState({ submitted: true });
        });
      }
    };
    return (
      <FormContainer>
        <Form>
          {({ formState }) => (
            <div>
              <label>
                <span className="testing">Name:</span>
                <Text field="name" required />
              </label>

              <label>
                Email:
                <Text field="email" required />
              </label>
              <label>
                Message:
                <TextArea field="message" required />
              </label>

              {this.state.submitted ? (
                <p>Your message has been sent!</p>
              ) : (
                <button
                  type="submit"
                  onClick={e => {
                    e.preventDefault;
                    formSubmit(formState);
                  }}
                >
                  Send
                </button>
              )}
            </div>
          )}
        </Form>
      </FormContainer>
    );
  }
}

export default ContactForm;
