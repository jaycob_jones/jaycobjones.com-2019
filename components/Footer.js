import React from 'react';
import styled from 'styled-components';
import Container from './Container';
import { IoLogoInstagram } from 'react-icons/io';

const FooterSection = styled.div`
    padding: 10px 0;
    text-align:center;
    color: ${props => props.theme.colors.black};
    span{
        font-size: 2rem;
        line-height:1;
        position: relative;
        transform:translateY(4px);
        display: inline-block;
    }
`;

const Footer = () => {

    const d = new Date();
const n = d.getFullYear();
    return (
        <Container className='background-white'>
            <FooterSection>
            <span><a target='_blank' href='https://www.instagram.com/jaycobjones/'><IoLogoInstagram/></a></span> | &copy; Copyright {n}. All rights reserved.
            </FooterSection>
        </Container>
    );
};

export default Footer;