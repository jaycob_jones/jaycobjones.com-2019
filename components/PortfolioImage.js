import styled from 'styled-components';
import Button from './Button'

const PortContainer = styled.div`
        height: 400px;
        width: 100%;
       
        position: relative;
  img{
        width:100%;
        object-fit: cover;
        object-position: center;
        position: relative;
        max-height: 400px;
    }
    .overlay{
        position:absolute;
        overflow-y: scroll;
        top:0;
        left:0;
        width:100%;
        height:100%;
        max-height: 400px;
        background:rgba(11,33,234,0);
        z-index:1;
        padding:20px;
        opacity:0;
        transition: background 500ms ease;
        &:hover{
            opacity:1;
            transition: background 500ms ease;
            background:rgba(11,33,234,.91);
        }
    }
    .link{
      color:white;
      position:relative;
      ::after{
        content:' ';
        width: 20px;
        height:2px;
        position:absolute;
        background:white;
        top:10;
        left:0;
      }
    }
`;

const PortfolioImage = props => (

  <>
    
  <PortContainer>
 
    <div className='overlay'>
      
        <h3 className='color-white'>{props.title}</h3>
        <p className='color-white'>{props.text}</p>
        { props.link ? <a target='_blank' class='link' href={props.link}>View Website</a> : null}
    </div>
    <img src={props.src} />
    
  </PortContainer>
 
  </>


   
  
  
);

export default PortfolioImage;
