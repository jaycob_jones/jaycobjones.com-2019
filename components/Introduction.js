import Container from "./Container";
import StyledHeadline from "./StyledHeadline";
import styled from "styled-components";

const LineArtCSS = styled.div`
  position: absolute;
  top: 0;
  left: 80px;
  width: 120px;
  z-index: 1;

  .img {
    width: 100%;
  }
`;

const Section3x = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  margin-top: 60px;
  grid-column-gap: 20px;
  margin-bottom: 70px;

  @media (max-width: ${(props) => props.theme.breakpoints.m}) {
    grid-template-columns: 1fr;
  }

  .sectionTitle {
    font-size: 2.75rem;
  }
`;
const Introduction = () => (
  <Container id="specialities">
    <StyledHeadline label="Specialities"></StyledHeadline>
    <LineArtCSS>
      <img src="../static/line-art.svg" />
    </LineArtCSS>
    <Section3x>
      <div>
        <div className="sectionTitle">Web Development</div>
        <p>
          With a proven background in web development and a particular strength
          in front-end design, I can bring your vision to life. From dynamic
          e-commerce platforms to informative websites, I am adept in creating
          solutions that not only meet but exceed client expectations.
          Leveraging modern frameworks such as React and Next.js, I can craft
          fast, responsive, and user-friendly sites.
        </p>
      </div>
      <div>
        <div className="sectionTitle">Frontend Design</div>
        <p>
          Translating ideas into visually appealing and intuitive interfaces is
          my forte. I can design anything from a single web page to an intricate
          web application. My designs are grounded in the principles of UX/UI,
          ensuring a smooth and engaging user experience regardless of project
          size.
        </p>
      </div>
      <div>
        <div className="sectionTitle">
          Industry 4.0 & Event-Driven Manufacturing
        </div>
        <p>
          As an expert in implementing Industry 4.0 methodologies and
          event-driven manufacturing practices, I can guide your enterprise
          towards a future of digitalized operations. Harnessing the power of
          Apache Kafka ecosystems and proficient in managing complex structures
          like RESTful APIs and Apollo GraphQL, I create real-time, data-driven
          applications for efficient and responsive factory operations. By
          integrating these advanced systems with cloud applications such as AWS
          and Palantir, I can help your organization embrace a fully connected,
          automated, and data-driven manufacturing process.
        </p>
      </div>
    </Section3x>
    <Section3x>
      <div>
        <div className="sectionTitle">Digital Strategy</div>
        <p>
          In the rapidly evolving digital landscape, your business needs a
          unique online presence. I can help strategize and implement successful
          digital campaigns using SEO, social media, and paid digital ads. By
          transforming your existing campaign, I can help increase visibility,
          drive traffic, and bolster customer engagement.
        </p>
      </div>
      <div>
        <div className="sectionTitle">Frontend Design</div>
        <p>
          Translating ideas into visually appealing and intuitive interfaces is
          my forte. I can design anything from a single web page to an intricate
          web application. My designs are grounded in the principles of UX/UI,
          ensuring a smooth and engaging user experience regardless of project
          size.
        </p>
      </div>
      <div>
        <div className="sectionTitle">Agile Leadership</div>
        <p>
          As an experienced software engineering manager, I understand the
          importance of agile methodologies in project management. Whether it's
          coordinating with a remote team or navigating technical complexities
          in fast-paced settings, I am highly effective at employing Scrum
          practices to ensure projects are delivered on time and within scope.
        </p>
      </div>
    </Section3x>
  </Container>
);

export default Introduction;
