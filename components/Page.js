import React, { Component } from "react";
import { ThemeProvider, createGlobalStyle } from "styled-components";
import { theme, GlobalStyle } from "../styles/theme";
import CookieAlert from "./CookieAlert";
import ReactGA from "react-ga";

const GA = () => {
  ReactGA.initialize("UA-90432539-1")
  ReactGA.pageview(window.location.pathname)
}

class Page extends Component {

  componentDidMount(){
    GA
  }
  componentDidUpdate(){
    GA
  }
  render() {

    //ReactGA.pageview(window.location.pathname != undefined ? window.location.pathname : 'looking');
    return (
      <ThemeProvider theme={theme}>
        <div>
          <GlobalStyle />
          <CookieAlert />

          {this.props.children}
        </div>
      </ThemeProvider>
    );
  }
}
export default Page;
