import Container from "./Container";
import Nav from "./Nav";
import styled from "styled-components";
import Button from "./Button";
import styled from "styled-components";
import Button from "./Button";

const SiteBanner = styled.div`
  position: relative;
  height: 100vh;
  h1 {
    margin-top: 60px;
    margin-bottom: 0;
    text-shadow: 2px 2px 20px rgba(0, 0, 0, 0.7);
    line-height: 1.2;
  }
  h4 {
    line-height: 1.2;
  }
  .section-divider {
    position: absolute;
    bottom: -15px;
    left: -60px;
    width: 130%;
  }
  .divider {
    width: 60px;
    height: 2px;
    background-color: ${(props) => props.theme.colors.white};
    margin: 30px 0;
  }
  .feature-image {
    position: absolute;
    bottom: 0;
    right: -5;
    width: 150vw;
    opacity: 0.15;
    pointer-events: none;
  }
`;

const Banner = ({ stars }) => (
  <Container className="background-primaryColor">
    <SiteBanner>
      <Nav />
      <img
        alt="Jaycob Jones Freelance"
        className="feature-image"
        src="../static/1101.png"
      />
      <h1 className="color-white">Hi, I'm Jaycob Jones</h1>
      <h4 className="color-white padding-left-20">
        I'm a Mechanical Engineer located in Reno, NV.
      </h4>
      <div className="divider">&nbsp;</div>
      <Button href="#contact" label="Hire Now" />
      <img className="section-divider" src="../static/section-divider.svg" />
    </SiteBanner>
  </Container>
);
export default Banner;
