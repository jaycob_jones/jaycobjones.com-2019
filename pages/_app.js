import React from "react";
import App, { Container } from "next/app";
import Page from "../components/Page";
import Head from 'next/head'


class MyApp extends App {
  static async getInitialProps(appContext) {
    const appProps = await App.getInitialProps(appContext);
    return { ...appProps };
  }

  render() {
    const { Component, pageProps } = this.props;
    return (
      <>
        <Head>
          <link href="https://fonts.googleapis.com/css?family=EB+Garamond|Oswald&display=swap" rel="stylesheet"/>
        </Head>
        <Page>

          <Component {...pageProps} />
        </Page>
      </>
    );
  }
}

export default MyApp;
