import React, { Component } from 'react'
import Banner from '../components/Banner'
import Introduction from '../components/Introduction'
import Work from '../components/Work'
import Contact from '../components/Contact'
import Footer from '../components/Footer'
class Index extends Component {
    // static async getInitialProps(ctx) {
    //     //0gjTWeQXBUvcOFNW
    //     //8677tmlai8u2w5
    //     const accessToken = '0gjTWeQXBUvcOFNW';

    //     const res = await fetch('https://api.linkedin.com/v1/people/~',{
    //         method: 'GET',
    //         headers: {
    //             'Authorization': `Bearer ${accessToken}`,
   
    //           },
    //     });

    //     console.log(res)

    //     const json = await res.json()
       
    //     return { stars: json}
    //   }
    render(){
        return(
            <>
            <Banner/>
            <Introduction/>
            <Work />
            <Contact/>
            <Footer />
            </>
        )
    }
}

export default Index