import Container from "../../components/Container";
import StyledHeadline from "../../components/StyledHeadline";
import styled from "styled-components";
import Link from "next/link";

const LineArtCSS = styled.div`
  position: absolute;
  top: 0;
  left: 80px;
  width: 120px;
  z-index: 1;

  .img {
    width: 100%;
  }
`;

const CheckIt = () => {
  return (
    <Container>
      <h2>Links</h2>
      <ul>
        <li>
          <Link href="/checkit/dianda">Dianda</Link>
        </li>
        <li>
          <Link href="/checkit/eldorado">Eldorado Resorts</Link>
        </li>
        <li>
          <Link href="/checkit/haveaheart">Have A Heart</Link>
        </li>
        <li>
          <Link href="/checkit/fireandice">Fire and Ice</Link>
        </li>
      </ul>
    </Container>
  );
};

export default CheckIt;
