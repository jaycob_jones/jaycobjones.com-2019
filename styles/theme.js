import styled, { createGlobalStyle } from "styled-components";

export const theme = {
  colors: {
    primaryColor: "#0b21ea",
    secondaryColor: "#393939",
    black: "#2f2f2f",
    white: "#ffffff",
    buttonOn: "#3b4eff",
    lightGrey: "#eaeaea",
  },
  default: {
    siteContainer: "1400px",
  },
  breakpoints: {
    s: "500px",
    m: "750px",
    l: "1200px",
    xl: "1600px",
  },
};

export const GlobalStyle = createGlobalStyle`
  html {
        box-sizing:border-box;
        font-size:10px;   
        font-family: 'Oswald', sans-serif;
        color:${theme.colors.black}; 
    }
    *,*:before,*:after{
     box-sizing:inherit;
    }
    body {
        padding:0;
        margin:0;
        font-size:1.5rem;
        line-height:2;
    }
    a{
        text-decoration: none;
        color:${theme.colors.black};
    }
    h1{
        font-size:7rem;
        padding: 10px 0;
        margin:0;
    }
    h3{
        font-size:3rem;
        padding: 10px 0;
        margin:0;
    }
    h4{
        font-size:2.7rem;
        font-family: 'EB Garamond', serif;
        padding: 6px 0;
        margin:0;
    }
    p{
        font-family: 'EB Garamond', serif;
        font-size:1.75rem;
    }
    .color-white{
        color:${theme.colors.white};
    }
    .background-primaryColor{
        background: ${theme.colors.primaryColor};
    }
    .background-lightGrey{
        background: ${theme.colors.lightGrey}
    }
    .background-lightPrimaryColor{
        background: ${theme.colors.buttonOn}
    }
    .padding-left-20{
        padding-left:20px;
    }
    .text-center{
        text-align:center;
    }
    .max-width-60{
        max-width:60%;
    }
`;
